% Copyright © 2019 Viviane Timmermann, Simula Research Laboratory, Norway

function Stretch
load('5%Heterogeneity/4_2mMol_Beat1.mat')
index = find(Tfinal>=600);

y = yFinal(index,:);
t = Tfinal(index);

if sum(y(:,1) > -84)>0
    for segments = 1:50
        if sum(y(:,38+(segments-1)*51)>=0.5*10^(-3))~=0
            ind = find(y(:,38+(segments-1)*51)>.5*10^(-3));
            
            [pks1,locs1] = findpeaks(y(ind,38+(segments-1)*51));
            pks(segments) = max(pks1);
            locs(segments) = locs1(find(max(pks1)==pks1));
            TimePks(segments) = t(ind(locs(segments)));
        end
    end
end

for Timings = min(TimePks)-200
    HzStretch(Timings);
end
