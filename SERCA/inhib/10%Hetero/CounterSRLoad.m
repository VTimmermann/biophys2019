function CounterSRLoad

for i = 1:6
    filename  = strcat('Hetero',num2str(i-1));
    cd(filename)
    load AnalysisSRmax.mat
    
    ReleaseTimingHelp(1:length(Counter),i) = Counter(:);
    SRHelp(1:length(SR),i) = SR;
    cd ..
end

[B,Ind] = sort(ReleaseTimingHelp(1,:));

for j=1:length(Ind)
    ReleaseTimingHelp2(1:length(ReleaseTimingHelp(:,Ind(j))),j) = ReleaseTimingHelp(:,Ind(j));
    SRHelp2(1:length(SRHelp(:,Ind(j))),j) = SRHelp(:,Ind(j));
end

ReleaseTiming = zeros(size(ReleaseTimingHelp2));
SR = zeros(size(SRHelp2));

ReleaseTiming(1:length(ReleaseTimingHelp2(:,1)),1) = ReleaseTimingHelp2(1:length(ReleaseTimingHelp2(:,1)),1);
SR(1:length(SRHelp2(:,1)),1) = SRHelp2(1:length(SRHelp2(:,1)),1);

for k = 2:length(B)
    if B(k-1) ~= B(k)        
        counter = 1;
        while ReleaseTimingHelp2(1,k) ~= ReleaseTiming(counter,k-1)
            counter = counter +1;
        end
        ReleaseTiming(counter:end,k) = ReleaseTimingHelp2(1:length(ReleaseTimingHelp2(:,k))-counter+1,k);
        SR(counter:end,k) = SRHelp2(1:length(SRHelp2(:,k))-counter+1,k);
    else
        counter = 1;
        while ReleaseTiming(counter,k-1) == 0
            counter = counter+1;
        end
        ReleaseTiming(counter:end,k) = ReleaseTimingHelp2(1:length(ReleaseTimingHelp2(:,k))-counter+1,k);
        SR(counter:end,k) = SRHelp2(1:length(SRHelp2(:,k))-counter+1,k);
    end
end

for o = 1:length(SR')
    Occurrence = length(find(SR(o,:)~=0));
    MeanSR(o) = sum(SR(o,:))./Occurrence;
    IndStd = find((SR(o,:))~=0);
    Std(o) = std(SR(o,IndStd));
end

save AnalysisSRTiming

hold on
plot(ReleaseTiming(:,1),MeanSR,'r')
plot(ReleaseTiming(:,1),MeanSR-Std,'ro-')
plot(ReleaseTiming(:,1),MeanSR+Std,'ro-')