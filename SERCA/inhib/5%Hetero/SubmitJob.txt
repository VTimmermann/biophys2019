#!/bin/sh
# 
# EXAMPLE OPEN MPI SCRIPT FOR SGE
# Modified by Basement Supercomputing 1/2/2006 DJE
# Modified by cmrg 19/June/2008 FVL

# Your job name 
#$ -N Wave

# Email options
#$ -M viviane@simula.no
#$ -m ae

# Use Verbose
#$ -V

# Use current working directory
#$ -cwd

# Join stdout and stderr
#$ -j y

# Use our GPU queue, which uses GPU and CPU nodes
#$ -q all.q

# To use CUDA nodes only
# -l cuda

# Memory usage
#$ -l h_vmem=20G

# Set your number of processors here. 
# Requests mpich environment although we actually are using openmpi
#$ -pe orte 1

# Run job through bash shell
#$ -S /bin/bash

# Export Library path
module load matlab
# Use full pathname to make sure we are using the right mpirun
matlab -nodesktop -nodisplay -nosplash < RunWave1.m
