function [Tfinal, yFinal] = Hz1()
    segments = 50;

    %% settings for simulation
    Tfinal = [];
    yFinal = [];
    
    tic;
    %% Run Simulation
    for Hetero = 1:6
        for counter = [300:50:1000]

            cd Heterogeneity
            SarcRefName = strcat('SarcRef',num2str(Hetero),'.mat');
            load(SarcRefName)
            InitName = strcat('4_2mMol_Init',num2str(Hetero),'.mat');
            load(InitName)
            cd ..
            load('xOpti.mat');
            Tfinal = [];
            yFinal = [];
            CL = 3000;
            tspan = [0:0.5:CL];

            options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','off'); 
            [t,y] = ode15s(@ODE,tspan,y0,options,segments,counter,sarc_ref,xOpti);

            Tfinal = [Tfinal;t];
            yFinal = [yFinal;y];

            toc

            clearvars -except yFinal Tfinal counter segments Hetero
            filename = strcat('4mMol_Stretch_',num2str(counter)); 
            save(filename)
        end
    end
end



