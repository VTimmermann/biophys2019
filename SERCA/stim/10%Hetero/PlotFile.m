function PlotFile
clear all

for Counter = 300:50:1000
    filename = strcat('4mMol_Stretch_',num2str(Counter));
    load(filename)
    
    hold on
    plot(Tfinal, yFinal(:,1))
end
