function SRLoad
close all
k= 1;
for counter = [300:50:1000]
    filename = strcat('4mMol_Stretch_',num2str(counter)); 
    load(filename)
    
    index = find(Tfinal<=700);

    y = yFinal(index,:);
    t = Tfinal(index);
    
    DAD(k) = max(y(:,1)); 
    SR_min(k) = min(sum(transpose(y(:,32:51:end)))).*50^(-1); 
    SR(k) = max(sum(transpose(y(:,32:51:end)))).*50^(-1);
    CaT(k) = max(sum(y(:,38:51:end)'))./50;
    
    k= k+1;
end

Counter = [300:50:1000];

clear yFinal Tfinal segments counter k y t index;
save AnalysisSRmax