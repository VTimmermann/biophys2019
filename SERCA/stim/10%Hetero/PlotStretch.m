function PlotStretch
close all
for counter = [300,1000]
    filename = strcat('4mMol_Stretch_',num2str(counter)); 
    load(filename)
    
    hold on
    plot(Tfinal,yFinal(:,1));
end