% Copyright © 2019 Viviane Timmermann, Simula Research Laboratory, Norway

function ErrorAnalysis

load('5%Heterogeneity/4mMol_Init1.mat') %Initial Conditions after 150 beats
load('5%Heterogeneity/SarcRef1.mat')
load('xOpti.mat');

%% settings for simulation
segments = 50;
beatEnd = 1;
CL = 600;
tspan = [0:0.1:CL];
Tfinal = [];
yFinal = [];
ErrorAP = 1;

tic;
%% Run Simulation
while ErrorAP>=0.01
    beats

    options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','off'); 
    [t,y] = ode15s(@ODE,tspan,y0,options,segments,sarc_ref,xOpti);
    y0 = y(end,:);

    Tfinal = [Tfinal;t+(beats-1)*CL];
    yFinal = [yFinal;y];

    if beats>2
        deltaSignal = (yFinal(1+6001*(beats-1):6001+6001*(beats-1),1)-yFinal(1+6001*beats:6001+6001*beats,1));
        percentageDifference = abs(deltaSignal ./ yFinal(1+6001*(beats-1):6001+6001*(beats-1),1));
        ErrorAP = 1/length(yFinal(1+6001*(beats-1):6001+6001*(beats-1),1))*sum(percentageDifference)
    end
    beats = beats+1;
        
end
toc

clearvars -except y0 yFinal Tfinal
filename = strcat('4mMol_Error'); 
save(filename)

    