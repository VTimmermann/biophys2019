% Copyright © 2019 Viviane Timmermann, Simula Research Laboratory, Norway

function [Tfinal, yFinal] = Hz( )
    segments = 50;

    % Reference EP model:
    % Shannon, Thomas R., et al. 
    % "A mathematical treatment of integrated Ca dynamics within the ventricular myocyte." 
    % Biophysical journal 87.5 (2004): 3351-3371.
    
    % Reference Mechanics model:
    % Rice, John Jeremy, et al. 
    % "Approximate model of cooperative activation and crossbridge cycling in cardiac muscle using ordinary differential equations." 
    % Biophysical journal 95.5 (2008): 2368-2390.
    
    % Reference extention to 1D:
    % Voigt, Niels, et al. 
    % "Cellular and molecular mechanisms of atrial arrhythmogenesis in patients with paroxysmal atrial fibrillation." 
    % Circulation 129.2 (2014): 145-156.
    
    load('5%Heterogeneity/4mMol_Init1.mat')
    load('5%Heterogeneity/SarcRef1.mat')
    load('xOpti.mat');
     
    %% settings for simulation
    beatEnd = 1;
    CL = 600;
    tspan = [0;CL];
    Tfinal = [];
    yFinal = [];
    
    tic;
    %% Run Simulation
    for beats = 1:beatEnd
        beats
        
        options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','off'); 
        [t,y] = ode15s(@ODE,tspan,y0,options,segments,sarc_ref,xOpti);
        y0 = y(end,:);
        
        if beats >= beatEnd-10
            Tfinal = [Tfinal;t+(beats-1)*CL];
            yFinal = [yFinal;y];
        end
        
    end
    toc
    y0 = y(end,:);
    clearvars -except y0 yFinal Tfinal
    filename = strcat('4mMol_Opti'); 
    save(filename)
    
    load('HeterogeneityFiles/SarcRef1.mat')
    load('xOpti.mat');
    segments = 50;
    
    for beats = 1:1
        beats
        
        options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','off'); 
        [t,y] = ode15s(@ODE,[0:0.1:3000],y0,options,segments,sarc_ref,xOpti);
        y0 = y(end,:);
        
            Tfinal = [t];
            yFinal = [y];
        
    end
    y0 = y(end,:);
    clearvars -except y0 yFinal Tfinal
    filename = strcat('4mMol_Beat'); 
    save(filename)
end



