% Copyright © 2019 Viviane Timmermann, Simula Research Laboratory, Norway

function [Tfinal, yFinal] = HzStretch(Begin, End)
    segments = 50;

    % Reference EP model:
    % Shannon, Thomas R., et al. 
    % "A mathematical treatment of integrated Ca dynamics within the ventricular myocyte." 
    % Biophysical journal 87.5 (2004): 3351-3371.
    
    % Reference Mechanics model:
    % Rice, John Jeremy, et al. 
    % "Approximate model of cooperative activation and crossbridge cycling in cardiac muscle using ordinary differential equations." 
    % Biophysical journal 95.5 (2008): 2368-2390.
    
    % Reference extention to 1D:
    % Voigt, Niels, et al. 
    % "Cellular and molecular mechanisms of atrial arrhythmogenesis in patients with paroxysmal atrial fibrillation." 
    % Circulation 129.2 (2014): 145-156.
     
    tic;
    %% Run Simulation
    for counter = [Begin:10:End]
        counter
        
        load('5%Heterogeneity/4_2mMol_Init2.mat')
        load('5%Heterogeneity/SarcRef2.mat')
        load('xOpti.mat');
        
        Tfinal = [];
        yFinal = [];
        CL = 3000;
        tspan = [0:0.1:CL];
        
        options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','off'); 
        [t,y] = ode15s(@ODEStretch,tspan,y0,options,segments,counter,sarc_ref,xOpti);
        
        Tfinal = [Tfinal;t];
        yFinal = [yFinal;y];
        
        toc
        
        clearvars -except yFinal Tfinal counter segments
        filename = strcat('4mMol_Stretch_',num2str(counter)); 
        save(filename)
    end
end



