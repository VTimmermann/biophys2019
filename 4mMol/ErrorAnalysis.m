% Copyright © 2019 Viviane Timmermann, Simula Research Laboratory, Norway

function ErrorAnalysis

load('5%Heterogeneity/4mMol_Init1.mat') %Initial Conditions after 150 beats
load('5%Heterogeneity/SarcRef1.mat')
load('xOpti.mat');

%% settings for simulation
segments = 50;
beats = 1;
CL = 600;
tspan = [0:0.1:CL];
Tfinal = [];
yFinal = [];
MSE = 1;

tic;
%% Run Simulation
while MSE>=0.005
    beats
    
    options = odeset('RelTol',1e-5,'MaxStep',1,'Stats','off'); 
    [t,y] = ode15s(@ODE,tspan,y0,options,segments,sarc_ref,xOpti);
    y0 = y(end,:);

    Tfinal = [Tfinal;t+(beats-1)*CL];
    yFinal = [yFinal;y];

    if beats>2
        MSE = sum(((yFinal(end-6001*2:end-6001-3000,1))-(yFinal(end-6001:end-3000,1))).^2)*1/length((yFinal(end-6001*2:end-6001-3000,1))-(yFinal(end-6001:end-3000,1))) %mean squared error
        
%         plot(Tfinal(end-6001:end)-600,yFinal(end-6001:end,1))
%         hold on
%         plot(Tfinal(end-6001*2:end-6001),yFinal(end-6001*2:end-6001,1))
        
    end
    beats = beats+1;
        
end
toc

clearvars -except y0 yFinal Tfinal
filename = strcat('4mMol_Error'); 
save(filename)

    